import json, re, logging
import pandas as pd
import uproot3
from sklearn.model_selection import train_test_split
logger = logging.getLogger()


def readJson(filename):
	with open(filename, 'r') as file:
		data = json.load(file)
	return data


def printJson(data):
	print(json.dumps(data, sort_keys=True, indent=4, separators=(', ', ': '), ensure_ascii=False))


def popJsonVar(data, key):
	var = data[key]
	data.pop(key)
	return var


def getTreeNames(inFileName, regexList):
	matchedTrees = []
	dataFile = uproot3.open(inFileName)
	all_ttrees = dict(dataFile.allitems(filterclass=lambda cls: issubclass(cls, uproot3.tree.TTreeMethods)))
	for i in range(len(regexList)):
		reg_s = regexList[i]
		reg = re.compile(reg_s)
		for tree_key in all_ttrees:
			key_name = tree_key.decode("utf-8")
			results = reg.match(key_name)
			if results is not None:
				matchedTrees.append(key_name)
	# duplicate members removal
	matchedTrees = list(set(matchedTrees))
	return matchedTrees


	# Append the weight Var to the pd_array
	#wei_array.eval('weights = weighVar[0]*weighVar[1]', inplace=True)
#def loadData(ttree, trainVars, weighVar=None, largestAllowWei=None, noNegWei=False):
def loadData(ttree, trainVars,ifweight, largestAllowWei=None,noNegWei=False):
    pd_array = ttree.arrays(trainVars, outputtype=pd.DataFrame)
    pd_array.eval('met_sig = met/sumPtLep', inplace=True) #to add new variables 
    pd_array.drop('sumPtLep', inplace=True, axis=1) # to drop intermediate variable
    
    if ifweight:
        wei_array = ttree.arrays(['totweight',"lumiScaling"], outputtype=pd.DataFrame)
        wei_array.eval('weights = totweight*lumiScaling', inplace=True)
        totweight = wei_array["weights"].sum()
        print("total ",totweight)
        pd_array = pd.concat([pd_array, wei_array['weights']], axis=1)
        if noNegWei is True:
            pd_array = pd_array[(pd_array['weights']>0)]
            if largestAllowWei is not None:
                threshold = abs(largestAllowWei)
                pd_array = pd_array[(pd_array['weights'] <= threshold) & (pd_array['weights'] >= (-threshold))]
    else:
        pd_array['weights'] = 1
    
    #pd_array.to_csv("data.csv") # to get a csv file 
    print(pd_array.head())
    return pd_array
     
#
def loadAllData(inFileName, trainList, trainVars,ifweight, weighVar, largestAllowWei, noNegWei):
    logger.info("Loading data form: %s", inFileName)
    # Get all trees of the input file
    dataFile = uproot3.open(inFileName)
    all_ttrees = dict(dataFile.allitems(filterclass=lambda cls: issubclass(cls, uproot3.tree.TTreeMethods)))

    logger.info("Loading the data group from the train list! Please make sure your signal is in the last group!")
    if noNegWei is True:
        logger.info("The noNegWei option is set. Will exclude events with weights <= 0")
    if largestAllowWei is not None:
        logger.info("The largestAllowWei option is set. Will exclude events with |weights| > %f", largestAllowWei)
    outData = pd.Series()

    for label in range(len(trainList)):
        sampleRes = trainList[label]
        matchedTrees = getTreeNames(inFileName, sampleRes)
        for key_name in matchedTrees:
            logger.info("Tree %s included as train. Label is: %s", key_name, label)
            events = all_ttrees[key_name.encode("utf-8")]
            # get the pandas array of the target tree
            pd_array = loadData(events, trainVars,ifweight, largestAllowWei, noNegWei)
            features = list(pd_array)
            print(features)
            # Append the label Var to the pd_array
            pd_array['label'] = label
            # merge the array to the outData
            if not outData.empty:
                outData = pd.concat([outData, pd_array], axis=0)
            else:
                outData = pd_array
    return [outData,features[:len(features)]]


def prepareTrainTestVali(data, trainSize):
	# data structure: [var1, var2, var3, ... , weight, label]
	# Standardize variables in datasets
	Y = data.label  # The label of each data group
	data.drop('label', inplace=True, axis=1)
	X_total, X_test, Y_total, Y_test = train_test_split(data, Y, train_size=trainSize, stratify=Y)
	X_train, X_vali, Y_train, Y_vali = train_test_split(X_total, Y_total, stratify=Y_total)

	# Then we get the weight of the training sample
	X_train_Wei = X_train.weights
	X_train.drop('weights', inplace=True, axis=1)
	X_test_Wei = X_test.weights
	X_test.drop('weights', inplace=True, axis=1)
	X_vali_Wei = X_vali.weights
	X_vali.drop('weights', inplace=True, axis=1)
	return X_train, X_train_Wei, Y_train, X_test, X_test_Wei, Y_test, X_vali, X_vali_Wei, Y_vali
